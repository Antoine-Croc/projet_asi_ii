package com.cpe.springboot.card.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.card.model.CardReference;

@Service
public class CardReferenceService {
	
	private final CardRefRepository cardRefRepository;
	
	public CardReferenceService(CardRefRepository cardRefRepository) {
		this.cardRefRepository=cardRefRepository;
	}

	public List<CardReference> getAllCardRef() {
		List<CardReference> cardRefList = new ArrayList<>();
		cardRefRepository.findAll().forEach(cardRefList::add);
		return cardRefList;
	}

	public void addCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);
	}

	public void updateCardRef(CardReference cardRef) {
		cardRefRepository.save(cardRef);

	}

	public CardReference getRandCardRef() {
		List<CardReference> cardRefList=getAllCardRef();
		if( cardRefList.size()>0) {
			Random rand=new Random();
			int rindex=rand.nextInt(cardRefList.size()-1);
			return cardRefList.get(rindex);
		}
		return null;
	}

	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
			CardReference cardRef =new CardReference("Dark Magician",
					"The ultimate wizard in terms of attack and defense.",
					"Yu-Gi-Oh",
					"dark",
					"https://storage.googleapis.com/ygoprodeck.com/pics/46986421.jpg",
					"https://www.nicepng.com/png/detail/71-713842_transparent-yugioh-logo-yu-gi-oh-the-art.png");
			addCardRef(cardRef);

			cardRef =new CardReference("Elemental HERO Avian",
			"A winged Elemental HERO who wheels through the sky and manipulates the wind. His signature move, Featherbreak, gives villainy a blow from sky-high.",
			"Yu-Gi-Oh",
			"Wind",
			"https://storage.googleapis.com/ygoprodeck.com/pics/21844576.jpg",
			"https://www.nicepng.com/png/detail/71-713842_transparent-yugioh-logo-yu-gi-oh-the-art.png");
			addCardRef(cardRef);

			cardRef =new CardReference("Elemental HERO Burstinatrix",
			"A flame manipulator who was the first Elemental HERO woman. Her Burstfire burns away villainy.",
			"Yu-Gi-Oh",
			"Fire",
			"https://storage.googleapis.com/ygoprodeck.com/pics/58932615.jpg",
			"https://www.nicepng.com/png/detail/71-713842_transparent-yugioh-logo-yu-gi-oh-the-art.png");
			addCardRef(cardRef);

			cardRef =new CardReference("Elemental HERO Bubbleman",
			"If this is the only card in your hand, you can Special Summon it (from your hand). When this card is Summoned: You can draw 2 cards. You must control no other cards and have no cards in your hand to activate and to resolve this effect.",
			"Yu-Gi-Oh",
			"Water",
			"https://storage.googleapis.com/ygoprodeck.com/pics/79979666.jpg",
			"https://www.nicepng.com/png/detail/71-713842_transparent-yugioh-logo-yu-gi-oh-the-art.png");
			addCardRef(cardRef);

			cardRef =new CardReference("Elemental HERO Clayman",
			"An Elemental HERO with a clay body built-to-last. He'll preserve his Elemental HERO colleagues at any cost.",
			"Yu-Gi-Oh",
			"Earth",
			"https://storage.googleapis.com/ygoprodeck.com/pics/84327329.jpg",
			"https://www.nicepng.com/png/detail/71-713842_transparent-yugioh-logo-yu-gi-oh-the-art.png");
			addCardRef(cardRef);

	}
	
}
