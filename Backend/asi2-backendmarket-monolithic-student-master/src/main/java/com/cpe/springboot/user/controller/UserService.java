package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.cpe.springboot.card.model.CardModel;
import com.cpe.springboot.common.tools.DTOMapper;
import com.cpe.springboot.card.model.CardDTO;
import com.cpe.springboot.user.model.UserDTO;
import com.cpe.springboot.user.model.UserModel;
@Service
public class UserService {

	private final UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	public void addUser(UserDTO user) {
		UserModel u = fromUDtoToUModel(user);
		// needed to avoid detached entity passed to persist error
		userRepository.save(u);

		RestTemplate restTemplate =new RestTemplate();
		Set <CardModel> listId = new HashSet<CardModel>();
		ResponseEntity<CardDTO[]> response = restTemplate.getForEntity("http://localhost:8082/cards/five/", CardDTO[].class);
		
		CardDTO[] cartes = response.getBody();

		for (int i=0; i< cartes.length ; i++) {
			u.addCard(DTOMapper.fromCardDtoToCardModel(cartes[i]));
			
				
		}
		userRepository.save(u);
	}

	public void updateUser(UserDTO user) {
		UserModel u = fromUDtoToUModel(user);
		userRepository.save(u);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist = null;
		ulist = userRepository.findByLoginAndPwd(login, pwd);
		return ulist;
	}

	private UserModel fromUDtoToUModel(UserDTO user) {
		UserModel u = new UserModel(user);
		RestTemplate restTemplate =new RestTemplate();
		
		List<CardDTO[]> cardList = new ArrayList<CardDTO[]>();
		for (Integer cardId : user.getCardList()) {
			ResponseEntity<CardDTO[]> response = restTemplate.getForEntity("http://localhost:8082/cartes/"+cardId, CardDTO[].class);
			
			CardDTO[] card = response.getBody();
			
			if(card.length == 1 ) {
				cardList.add(card);
			}

			
		}
		return u;
	}

}
