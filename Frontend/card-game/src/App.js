import './App.css';
import MainRouter from './parts/main/MainRouter.js';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import globalReducer from './reducers';

//create store to exchange data
const store = createStore(globalReducer);

function App() {
  return (
    <Provider store={store}>
          <MainRouter></MainRouter>
    </Provider>
  );
}

export default App;
