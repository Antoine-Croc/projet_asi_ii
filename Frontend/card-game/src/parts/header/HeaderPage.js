import { NavLink } from "react-router-dom";
function HeaderPage(props) {
  const title = props.title
  let user = props.user
    return (
      <div className="container">
        <div className="row" id="rowHeader">
          <div className="col">
            <div>
            <NavLink to="/market" ><p id="soldPlayer">{user.account}$</p></NavLink>
            <NavLink to="/duel" ><p id="soldPlayer">Go Fight</p></NavLink>
            </div>
          </div>
          <div className="col">
            <p id="titleHeader"><h1>{title}</h1></p>
          </div>
          <div className="col">
            <div id="profilPlayer">
              <div className="contentProfil"><NavLink to="/collection" ><img id="pdprofil" src="https://sbcf.fr/wp-content/uploads/2018/03/sbcf-default-avatar.png"></img></NavLink></div>
              <div className="contentProfil"><p id="nameprofil">{user.login}</p></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
  export default HeaderPage;