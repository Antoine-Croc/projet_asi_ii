import Hand from './Hand'
import HeaderPage from '../../header/HeaderPage';
import {useSelector} from 'react-redux';
import Area from './Area'
import React,{ useEffect, useState } from 'react'
import axios from 'axios'
import Carte from '../Carte';
import AdversInfo from './AdversInfo';
    
function Duel() {
    let user = useSelector(state => state.userReducer.current_user)
    const [carte, setCarte] = useState();
    const [cartes,setCartes] = useState([]);

    function onSelectCarte(id){
        for(var carteTest of cartes)
        {
            
            if(id == carteTest.id)
            {
                console.log(carteTest)
                setCarte(carteTest);
            }
        }
    }

    function isReadyToDisplay(){
        let size = Object.keys(user.cardList).length;
        if (size== cartes.length)
        {
            setCartes([...cartes]);
        }
    }
    useEffect (() => {
        const url = 'http://127.0.0.1:8082/card/';
        for(var id of user.cardList)
        {
            axios.get(url+id).then(res => {
            cartes.push(res.data);
            isReadyToDisplay();
            });
        }
    },[]);
    let display;
    if(carte != undefined) {
        display=(<Carte carte={carte}></Carte>);
    }

    return(
        <div>
            <div className="row">
                <HeaderPage user={user} title="DUEL"></HeaderPage>
            </div>
            <div className="row">
                <div className="col-sm-2">
                </div>
                <div className="col-sm-8">
                    <p> The Adversaire : </p>
                    <AdversInfo></AdversInfo>
                    <p> The BattelField : </p>
                    <Area></Area>
                    <p> Your Hand : </p>
                    <Hand cartes={cartes} onSelectCarte={onSelectCarte}></Hand>
                </div>
                <div className="col-sm-2">
                    {display}
                </div>
            </div>
        </div>
            
    )
}
export default Duel;