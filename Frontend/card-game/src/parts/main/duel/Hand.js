import  {useSelector} from 'react-redux';
import React,{ useEffect, useState } from 'react';

function Hand(props) {
    let user = useSelector(state => state.userReducer.current_user)
    let cartes = props.cartes

    function displayCards() {
        let displayTemp = cartes.map(
            (carte) => 
                <div className="card cardHand" onClick={() => {props.onSelectCarte(carte.id)}}>
                    <div className="content">
                        <div className="ui grid">
                            <div className="three column row">
                                <div className="column">
                                        <a className="ui red circular label">{carte.attack}</a>
                                </div>
                                <div className="column" >
                                        <h5>{carte.name}</h5>
                                </div>
                                <div className="column">
                                        <a className="ui yellow circular label">{carte.defence}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="image imageCard">
                        <div className="ui fluid image">
                            <img id="cardImgId" className="ui centered image" src={carte.smallImgUrl}></img>
                        </div>
                    </div>
                </div>
        )
      return displayTemp;
    }


    let current_dispay=displayCards();

    return(
        <div className="row">
            {current_dispay}
        </div>
            
    )
}
export default Hand;