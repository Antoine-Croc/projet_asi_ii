function Carte(props) {
  let carte = props.carte;
  console.log(carte)
  return (
    <div className="ui special cards">
      <div className="card">
        <div className="content">
          <div className="ui grid">
              <div className="three column row">
                  <div className="column"><i className="heart outline icon"></i><span id="cardHPId">{carte.HP}</span></div>
                  <div className="column"><h5>{carte.family}</h5></div>
                  <div className="column"><span id="energyId">{carte.energy}</span><i className="lightning icon"></i></div>
              </div>
          </div>
        </div> 

        <div className="image imageCard">
          <div className="blurring dimmable image">
              <div className="ui inverted dimmer">
                  <div className="content">
                      <div className="center"><div className="ui primary button">Add Friend</div></div>
                  </div>
              </div>

              <div className="ui fluid image">
                  <a className="ui left corner label">{carte.name}</a>
                  <img id="cardImgId" className="ui centered image" src={carte.imgUrl}></img>
              </div>
            </div>
        </div>
        
        <div className="content">
            <div className="ui form tiny">
                <div className="field">
                    <label id="cardNameId"></label>
                    <p id="cardDescriptionId" className="overflowHiden" >{carte.description}</p>
                </div>
            </div>
        </div>
  
        <div className="content">
            <i className="heart outline icon"></i><span id="cardHPId"> HP {carte.hp}</span> 
            <div className="right floated "><span id="cardEnergyId">Energy {carte.energy}</span><i className="lightning icon"></i></div>
        </div>

        <div className="content">
            <span className="right floated"><span id="cardAttackId"> Attack {carte.attack}</span><i className=" wizard icon"></i></span>
            <i className="protect icon"></i><span id="cardDefenceId">Defense {carte.defence}</span> 
        </div>

        <div className="ui bottom attached button">
            <i className="money icon"></i> Actual Value <span id="cardPriceId"> {carte.price}$</span>
        </div>

      </div>
    </div>
  );
}

export default Carte;
