import { BrowserRouter as Router, Route ,Link, Routes, Navigate } from "react-router-dom";
import Parts from './../Parts';
import {ConnectMain} from "./ConnectMain";
import {useSelector} from 'react-redux';
import WelcomeMain from './WelcomeMain'
import Duel from './duel/Duel'
function MainRouter() {
    let user = useSelector(state => state.userReducer.current_user)
    
    if(user.id) 
    {
        console.log("authentifié")
        var connected = true;
    }
    else 
    {
        var connected = false;
    }
    
      return (
        <Router>
            <Routes>
                <Route exact path="connexion" element={<ConnectMain/>} />
                <Route exact path="accueil" element={<WelcomeMain title="Welcome"></WelcomeMain>} />

                { connected && 
                    <Route exact path="market" element={<Parts title="MARKET"/>} />
                }
                { connected && 
                    <Route exact path="collection" element={<Parts title="COLLECTION"/>} />
                }
                { connected && 
                    <Route exact path="duel" element={<Duel/>} />
                }
                { connected && 
                    <Route path="*" element={<Navigate to="/accueil" />}/>
                }
                { !connected && 
                    <Route path="*" element={<Navigate to="/connexion" />}/>
                }
                
            </Routes>
        </Router>
      );
    }
    
    export default MainRouter;