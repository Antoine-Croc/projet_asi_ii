import React from 'react';
import {useState} from 'react';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import {updateUser} from '../../actions';

import { 
  NavLink
} from "react-router-dom";


export const ConnectMain = (props) =>{
  const [login,setLogin]=useState('');
  const [pwd,setPwd]=useState('');
  const [display,setDisplay]=useState('');

  const dispatch=useDispatch();

  function handleChange (event){
    setLogin(event.target.value );
  }
  
  function handleChangePwd( event ){
    setPwd(event.target.value );
  }

  function  handleSubmit( event ){
    event.preventDefault();

    const user = {  params: { login: login, pwd: pwd }};
    console.log('http://127.0.0.1:8082/auth?login='+login+'&pwd='+pwd)
    axios.post('http://127.0.0.1:8082/auth?login='+login+'&pwd='+pwd, JSON.stringify({'login': login,'pwd': pwd,}) , { headers: {  "Content-Type": "application/json"  }})

      .then(res => {
        console.log(res.data);
        if(res.data != -1) {
          axios.get(`http://127.0.0.1:8082/user/`+res.data)
            .then(res => {
              dispatch(updateUser(res.data));
              setDisplay(<div><span id="authentificationSucess"> Authentification sucess </span><br></br><NavLink to="/accueil" >Welcome</NavLink></div>)
            })
        }
        else {  
          setDisplay(<span id="authentificationFail"> Authentification fail </span>)
        }
        })
  }


  return (     
  <div id="bodyConnexion">
    <center><h1>Welcome to Yu-GASI</h1></center>
    <form name="form" id="form_id" onSubmit={handleSubmit}>
      <div class="container-fluid bg-secondary bg-gradient">

        <legend class="Title mx-2 my-3">Sign In</legend>

        <div class='row'>
          <div class="col-sm">
            <label>Login ID:</label>
            <input type="text" id="login" required onChange={handleChange}></input><br></br>
            <label>Password:</label>
            <input type="password" id="pwd" required onChange={handleChangePwd}></input>
          </div>
        <div class="col-sm" className="contentProfil">
          <NavLink to="/connexion" ><img id="pdprofil" src="https://imagizer.imageshack.com/img924/1055/uHkvLI.png"></img></NavLink>
        </div>
        </div>
        <div class="form-group">
          <button class="btn btn-danger mt-2" type="submit" id="submit">Login</button>
        </div>
        {display}
        <p>Forgot <a href="#"> password? </a></p>
      </div>


    </form>
  </div>

);


}

