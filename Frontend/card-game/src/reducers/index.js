import { combineReducers } from 'redux';
import userReducer from './userReducer';
import cardReducer from './cardReducer';

const globalReducer = combineReducers({
    userReducer: userReducer,
    cardReducer: cardReducer,
});

export default globalReducer;
