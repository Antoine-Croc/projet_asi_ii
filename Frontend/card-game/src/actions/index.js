export const updateUser =
    (user) => {
                return { 
                        type: 'UPDATE_USER_ACTION', 
                        user: user 
                       };
              }

export const selectedCard =
    (card) => {
                return { 
                        type: 'SELECTED_CARD_ACTION', 
                        card: card 
                       };
              }

export const updateCards =
(cardlist) => {
                return {
                        type: 'UPDATE_CARDS_ACTION',
                        cardlist:cardlist
                };
}
